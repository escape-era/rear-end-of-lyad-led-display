const express = require("express");
const router = express.Router();
const db = require("../mysql/index");

// 添加黑红线
router.post("/addhiehong", (req, res) => {

  
    // 插入用户语句
    //  插入前先判断是否已经有了相同的名称的
    const yong = "select model from hiehong where model =? and openid =?";
  
    db.query(yong, [req.body.name,req.user.id], (errs, rec) => {
      
      if (errs)
        return res.send({
          code: 404,
          message: "数据库黑红线插入请求失败",
        });
  
      // 已经存在品牌
      if (rec.length !== 0) {
        res.send({code: 201, message: "该黑红线线已经存在！"});
        // 不存在添加进去
      } else {
        const id = Math.floor(Math.random() * 1000000000) + 1;
        const charuUser =
          "insert into hiehong (id,model,price,status,sort,openid) values (?,?,?,?,?,?)";

         
        db.query(
          charuUser,
          [id, req.body.model,req.body.price, req.body.status, req.body.sort,req.user.id],
          (err, resb) => {
            if (err)
              return res.send({
                code: 404,
                message: "数据库请求失败",
              });
  
            if (resb.affectedRows == 1) {
              res.send({code: 200, message: "黑红线添加成功！"});
            }
          }
        );
      }
    });
  });

  //   获取黑红线线

router.post("/obtaihiehong", (req, res) => {
    // 获取总行数
    let hang = 0;
    const zong = "SELECT COUNT(*) FROM hiehong where openid = ?;";
    db.query(zong, req.user.id,(err, ress) => {
      hang = ress[0]["COUNT(*)"];
  
      // const sqlurl = "select * from control_card ORDER BY sort ASC";
      const sqlurlt =
        "select * from hiehong where status like ? and model like ? and openid = ? ORDER BY sort ASC LIMIT ?,?";
      let nun = req.body;
  
      let cha = {};
  
      if (nun.input == "") {
        cha.input = "%";
      } else {
        cha.input = nun.input;
      }
      if (nun.status == 0) {
        cha.status = "%";
      } else {
        cha.status = nun.status;
      }
      // 从哪里开始查询
      cha.currentPage = nun.pageSize * (nun.currentPage - 1);
      db.query(
        sqlurlt,
        [cha.status, cha.input,req.user.id, cha.currentPage, nun.pageSize],
        (err, ress) => {
          if (err)
            return res.send({
              code: 404,
              message: "数据库请求失败",
            });
  
          if (ress.length !== 0) {
            res.send({
              code: 200,
              data: ress,
              message: "获取黑红线成功！",
              total: hang,
            });
          } else if (ress.length == 0) {
            res.send({
              code: 200,
              data: ress,
              total: hang,
              message: "获取黑红线成功！",
            });
          }
        }
      );
    });
  });


    // 删除黑红线
    router.post('/deletehiehong',(req,res)=>{


        const deletepin='delete from hiehong where id=?'
        db.query(deletepin,req.body.id,(err,ress)=>{
    
          if(err) return res.send({
            code:404,
            message:'数据库删除黑红线线失败'})
            
            if(ress.affectedRows==1){
              res.send({
                code:200,
                message:'黑红线删除成功！'})
            }
    
            
        })
      })
// 编辑黑红线
router.post('/edithiehong',(req,res)=>{

    let nun=req.body
    const sqlurl = `update hiehong set model=?,price=?,status=?,sort=? where id=?`;
    db.query(sqlurl,[nun.model,nun.price,nun.status,nun.sort,nun.id],(err,ress)=>{
    if(err) return res.send({
      code:404,
      message:'数据库编辑黑红线失败'})
    if(ress.affectedRows==1){
      res.send({
        code:200,
        message:'编辑成功!'
      })
    }
    })
    })
// 批量删除

router.post('/batchDeletehiehong',(req,res)=>{
    const sqlurl="DELETE  FROM hiehong where id=?"
    
      let nun=req.body.nunId
      let num=0
      nun.forEach((item)=>{
        db.query(sqlurl,item,(err,ress)=>{
       
          if(err) return res.send({
            code:404,
            message:'数据库批量删除电源品牌失败'})
            if(ress.affectedRows==1){
              num++
            
        if(num==nun.length){
    res.send({code:200,
      message:'批量删除成功!'})
        }
            }
        })
    
      })
    })
// 创建获取聊天对象的接口
module.exports = router;