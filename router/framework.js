const express = require("express");
const router = express.Router();
const db = require("../mysql/index");


// 添加框架
router.post("/addFramework", (req, res) => {
    // 插入用户语句

      // 已经存在品牌
  
        const id = Math.floor(Math.random() * 1000000000) + 1;
        const charuUser =
          "insert into framework (id,model,edge,price,corners,Corner_price,framework_price,brand_id,openid) values (?,?,?,?,?,?,?,?,?)";
        db.query(
          charuUser,
          [id, req.body.model, req.body.edge, req.body.price,req.body.corners,
            req.body.Corner_price,req.body.framework_price,req.body.brand_id,req.user.id],
          (err, resb) => {
            console.log(err);
            if (err)
              return res.send({
                code: 404,
                message: "数据库请求失败",
              });
  
            if (resb.affectedRows == 1) {
              res.send({code: 200, message: "框架添加成功！"});
            }
          }
        );
  });
// 获取框架
router.post("/obtainFramework", (req, res) => {
    // 获取总行数
    let hang = 0;
    const zong = "SELECT COUNT(*) FROM framework where openid = ?;";
    db.query(zong,req.user.id, (err, ress) => {
      hang = ress[0]["COUNT(*)"];
  
      // const sqlurl = "select * from control_card ORDER BY sort ASC";
      const sqlurlt =
        "select * from framework where brand_id like ? and model like ? and openid = ?  LIMIT ?,?";
      let nun = req.body;
  
      let cha = {};
  
      if (nun.input == "") {
        cha.input = "%";
      } else {
        cha.input = nun.input;
      }
      if (nun.status == 0) {
        cha.status = "%";
      } else {
        cha.status = nun.status;
      }
      // 从哪里开始查询
      cha.currentPage = nun.pageSize * (nun.currentPage - 1);
      db.query(
        sqlurlt,
        [cha.status, cha.input, req.user.id,cha.currentPage, nun.pageSize],
        (err, ress) => {

            console.log(err);
          if (err)
            return res.send({
              code: 404,
              message: "数据库请求失败",
            });
  
          if (ress.length !== 0) {
            res.send({
              code: 200,
              data: ress,
              message: "获取框架成功！",
              total: hang,
            });
          } else if (ress.length == 0) {
            res.send({
              code: 200,
              data: ress,
              total: hang,
              message: "获取框架成功！",
            });
          }
        }
      );
    });
  });


    // 删除框架
    router.post('/deleteframework',(req,res)=>{


        const deletepin='delete from framework where id=?'
        db.query(deletepin,req.body.id,(err,ress)=>{
    
          if(err) return res.send({
            code:404,
            message:'数据库删除框架失败'})
            
            if(ress.affectedRows==1){
              res.send({
                code:200,
                message:'框架删除成功！'})
            }
    
            
        })
      })

// 编辑框架
router.post('/editFramework',(req,res)=>{

    let nun=req.body
    console.log(nun);
    // id,model,edge,price,corners,Corner_price,framework_price,status
    const sqlurl = `update framework set model=?,edge=?,price=?,corners=?,Corner_price=?,framework_price=?,brand_id=? where id=?`;
    db.query(sqlurl,[nun.model,nun.edge,nun.price,nun.corners,nun.Corner_price,nun.framework_price,nun.brand_id,nun.id],(err,ress)=>{
    
    console.log(err);
        if(err) return res.send({
      code:404,
      message:'数据库编辑框架失败'})
    if(ress.affectedRows==1){
      res.send({
        code:200,
        message:'编辑成功!'
      })
    }
    })
    })

// 批量删除电源品牌
router.post('/batchFramework',(req,res)=>{
    const sqlurl="DELETE  FROM framework where id=?"
    
      let nun=req.body.nunId
      let num=0
      nun.forEach((item)=>{
        db.query(sqlurl,item,(err,ress)=>{
       
          if(err) return res.send({
            code:404,
            message:'数据库批量删除框架失败'})
            if(ress.affectedRows==1){
              num++
            
        if(num==nun.length){
    res.send({code:200,
      message:'批量删除成功!'})
        }
            }
        })
    
      })
    })








// 创建获取聊天对象的接口
module.exports = router;