const express = require("express");
const router = express.Router();
const db = require("../mysql/index");

// 添加箱体
router.post("/addBox", (req, res) => {

    console.log(req.body);
    // 插入用户语句
    //  插入前先判断是否已经有了相同的名称的
    const yong = "select model from box where model =? and openid = ?";
  
    db.query(yong, [req.body.model,req.user.id] ,(errs, rec) => {
      
      if (errs)
        return res.send({
          code: 404,
          message: "数据库请求失败",
        });
  
      // 已经存在品牌
      if (rec.length !== 0) {
        res.send({code: 201, message: "该箱体类型已经存在！"});
        // 不存在添加进去
      } else {
        const id = Math.floor(Math.random() * 1000000000) + 1;
        const charuUser =
          "insert into box (id,model,edge,price,status,openid) values (?,?,?,?,?,?)";
        db.query(
          charuUser,
          [id, req.body.model, req.body.edge, req.body.price,req.body.status,req.user.id],
          (err, resb) => {
            console.log(err);
            if (err)
              return res.send({
                code: 404,
                message: "数据库请求失败",
              });
  
            if (resb.affectedRows == 1) {
              res.send({code: 200, message: "箱体添加成功！"});
            }
          }
        );
      }
    });
  });

// 获取箱体
router.post("/obtainbox", (req, res) => {
    // 获取总行数
    let hang = 0;
    const zong = "SELECT COUNT(*) FROM box where openid =?;";
    db.query(zong, req.user.id,(errs, ress) => {
      hang=ress?ress[0]['COUNT(*)']:0

      const sqlurlt =
        "select * from box where status like ? and model like ? and openid = ?  LIMIT ?,?";
      let nun = req.body;
  
      let cha = {};
  
      if (nun.input == "") {
        cha.input = "%";
      } else {
        cha.input = nun.input;
      }
      if (nun.status == 0) {
        cha.status = "%";
      } else {
        cha.status = nun.status;
      }
      // 从哪里开始查询
      cha.currentPage = nun.pageSize * (nun.currentPage - 1);
      db.query(
        sqlurlt,
        [cha.status, cha.input,req.user.id, cha.currentPage, nun.pageSize],
        (err, ress) => {
        
          if (err)
            return res.send({
              code: 404,
              message: "数据库请求失败",
            });
  
          if (ress.length !== 0) {
            res.send({
              code: 200,
              data: ress,
              message: "获取箱体成功！",
              total: hang,
            });
          } else if (ress.length == 0) {
            res.send({
              code: 200,
              data: ress,
              total: hang,
              message: "获取箱体成功！",
            });
          }
        }
      );
    });
  });

  // 删除箱体
  router.post('/deletebox',(req,res)=>{


    const deletepin='delete from box where id=?'
    db.query(deletepin,req.body.id,(err,ress)=>{

      if(err) return res.send({
        code:404,
        message:'数据库删除电源品牌失败'})
        
        if(ress.affectedRows==1){
          res.send({
            code:200,
            message:'电源品牌删除成功！'})
        }

        
    })
  })
// 编辑箱体
router.post('/editbox',(req,res)=>{
    // insert into box (id,model,edge,price,status) values (?,?,?,?,?)
    let nun=req.body
    console.log(req.body);
    // update power set name=?,status=?,sort=? where id=?
    const sqlurl = `update xiaochengxu.box set model=?,edge=?,price=?,status=? where id= ?`;
    db.query(sqlurl,[nun.model,nun.edge,nun.price,nun.status,nun.id],(err,ress)=>{
    
    console.log(nun.id);
        if(err) return res.send({
      code:404,
      message:'数据库编辑电源品牌失败'})
    if(ress.affectedRows==1){
      res.send({
        code:200,
        message:'编辑成功!'
      })
    }
    })
    })
// 批量删除电源品牌
router.post('/batchDeleteBox',(req,res)=>{
    const sqlurl="DELETE  FROM box where id=?"
    
      let nun=req.body.nunId
      let num=0
      nun.forEach((item)=>{
        db.query(sqlurl,item,(err,ress)=>{
       
          if(err) return res.send({
            code:404,
            message:'数据库批量删除箱体失败'})
            if(ress.affectedRows==1){
              num++
            
        if(num==nun.length){
    res.send({code:200,
      message:'批量删除成功!'})
        }
            }
        })
    
      })
    })







// 创建获取聊天对象的接口
module.exports = router;