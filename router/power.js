const express = require("express");
const router = express.Router();
const db = require("../mysql/index");




// 添加电源品牌
router.post("/addPower", (req, res) => {
    // 插入用户语句
    console.log(req.user.id);
    //  插入前先判断是否已经有了相同的名称的
    const yong = "select name from power where name =? and openid = ?";
  
    db.query(yong, [req.body.name,req.user.id], (errs, rec) => {
      if (errs)
        return res.send({
          code: 404,
          message: "数据库请求失败",
        });
  
      // 已经存在品牌
      if (rec.length !== 0) {
        res.send({code: 201, message: "该品牌已经存在！"});
        // 不存在添加进去
      } else {
        const id = Math.floor(Math.random() * 1000000000) + 1;
        const charuUser =
          "insert into power (id,name,status,sort,openid) values (?,?,?,?,?)";
        db.query(
          charuUser,
          [id, req.body.name, req.body.status, req.body.sort,req.user.id],
          (err, resb) => {
            if (err)
              return res.send({
                code: 404,
                message: "数据库请求失败",
              });
  
            if (resb.affectedRows == 1) {
              res.send({code: 200, message: "电源品牌添加成功！"});
            }
          }
        );
      }
    });
  });

// 获取电源品牌

router.post("/obtainPower", (req, res) => {
    // 获取总行数
    let hang = 0;
    const zong = "SELECT COUNT(*) FROM power where openid = ?;";
    db.query(zong,req.user.id, (err, ress) => {
      hang=ress?ress[0]['COUNT(*)']:0
  
      // const sqlurl = "select * from control_card ORDER BY sort ASC";
      const sqlurlt =
        "select * from power where status like ? and name like ? and openid= ? ORDER BY sort ASC LIMIT ?,?";
      let nun = req.body;
  
      let cha = {};
  
      if (nun.input == "") {
        cha.input = "%";
      } else {
        cha.input = nun.input;
      }
      if (nun.status == 0) {
        cha.status = "%";
      } else {
        cha.status = nun.status;
      }
      // 从哪里开始查询
      cha.currentPage = nun.pageSize * (nun.currentPage - 1);
      db.query(
        sqlurlt,
        [cha.status, cha.input,req.user.id, cha.currentPage, nun.pageSize],
        (err, ress) => {
          if (err)
            return res.send({
              code: 404,
              message: "数据库请求失败",
            });
  
          if (ress.length !== 0) {
            res.send({
              code: 200,
              data: ress,
              message: "获取电源品牌成功！",
              total: hang,
            });
          } else if (ress.length == 0) {
            res.send({
              code: 200,
              data: ress,
              total: hang,
              message: "获取电源品牌成功！",
            });
          }
        }
      );
    });
  });


     // 删除电源品牌
     router.post('/deletePower',(req,res)=>{


        const deletepin='delete from power where id=?'
        db.query(deletepin,req.body.id,(err,ress)=>{
    
          if(err) return res.send({
            code:404,
            message:'数据库删除电源品牌失败'})
            
            if(ress.affectedRows==1){
              res.send({
                code:200,
                message:'电源品牌删除成功！'})
            }
    
            
        })
      })

// 编辑电源品牌
router.post('/editPower',(req,res)=>{

    let nun=req.body
    const sqlurl = `update power set name=?,status=?,sort=? where id=?`;
    db.query(sqlurl,[nun.name,nun.status,nun.sort,nun.id],(err,ress)=>{
    if(err) return res.send({
      code:404,
      message:'数据库编辑电源品牌失败'})
    if(ress.affectedRows==1){
      res.send({
        code:200,
        message:'编辑成功!'
      })
    }
    })
    })

// 批量删除电源品牌
router.post('/batchDeletePower',(req,res)=>{
    const sqlurl="DELETE  FROM power where id=?"
    
      let nun=req.body.nunId
      let num=0
      nun.forEach((item)=>{
        db.query(sqlurl,item,(err,ress)=>{
       
          if(err) return res.send({
            code:404,
            message:'数据库批量删除电源品牌失败'})
            if(ress.affectedRows==1){
              num++
            
        if(num==nun.length){
    res.send({code:200,
      message:'批量删除成功!'})
        }
            }
        })
    
      })
    })

// 添加电源型号
router.post('/addPowerModel',(req,res)=>{
  
    // 插入用户语句
   //  插入前先判断是否已经有了相同的名称的
   const yong = "select model from power_model where model =? and openid= ?";

   db.query(yong,[req.body.model,req.user.id],(errs,rec)=>{
   if(errs) return res.send({
     code:404,
     message:'数据库请求电源型号失败'})
   
   // 已经存在品牌
   if(rec.length!==0){
   res.send({code:201,
     message:'该型号已经存在！'})
   // 不存在添加进去
   }else{
  
     let nun=req.body
     console.log(nun);
    const id=Math.floor(Math.random()*1000000000)+1
    const charuUser = "insert into power_model (id,model,price,brand_id,gonglv,openid) values (?,?,?,?,?,?)";
     db.query(charuUser,[id,nun.model,nun.price,nun.brand_id,nun.gonglv,req.user.id],(err,resb)=>{
      if(err) return res.send({
       code:404,
       message:'数据库请求添加电源型号失败'})
     if(resb.affectedRows==1){
       res.send({code:200,
         message:'电源型号添加成功！'}) }})} }) })

// 获取电源型号
router.post('/obtainPowerModel',(req,res)=>{
    // 获取总行数
  let hang=0
  const zong='SELECT COUNT(*) FROM power_model where openid = ? ;'
  db.query(zong,req.user.id,(err,ress)=>{
    hang=ress[0]['COUNT(*)']
  
      // const sqlurl = "select * from brand ORDER BY sort ASC";
      const sqlurlt = "select * from power_model where brand_id like ? and model like  ? and openid = ? LIMIT ?,?";
      let nun=req.body
    
      let cha={}
     
     if(nun.input==''){
     cha.input="%"
     
     }else{
       cha.input=nun.input
     }
     if(nun.status==0){
       cha.status="%"
     }else{
       cha.status=nun.status
     }
     

     // 从哪里开始查询
     cha.currentPage=nun.pageSize*(nun.currentPage-1)
     
       db.query(sqlurlt,[cha.status,cha.input,req.user.id,cha.currentPage,nun.pageSize],(err,ress)=>{
     
         if(err) return res.send({
           code:404,
           message:'数据库获取电源型号请求失败'})
     
           if(ress.length!==0){
     
             res.send({
               code:200,
               data:ress,
               message:'获取电源型号成功！',
               total:hang
             })
           }else if(ress.length==0){
             res.send({
               code:200,
               data:ress,
               total:hang,
               message:'获取电源型号成功！'
             })
           }
       })
  })
  
  })
// 编辑电源型号
router.post('/editPowerModel',(req,res)=>{
    let nun=req.body
  const sqlurl = `update power_model set model=?,price=?,brand_id=?,gonglv=? where id=?`;
  db.query(sqlurl,[nun.model,nun.price,nun.brand_id,nun.gonglv,nun.id],(err,ress)=>{
    if(err) return res.send({
    code:404,
    message:'数据库编辑处理器型号失败'})
  if(ress.affectedRows==1){
    res.send({
      code:200,
      message:'编辑处理器型号成功!'
    })
  }
  })
  })
// 删除处理器型号
router.post('/deletePowerModel',(req,res)=>{

  
    const deletepin='delete from power_model where id=?'
    db.query(deletepin,req.body.id,(err,ress)=>{
  
      if(err) return res.send({
        code:404,
        message:'数据库删除电源型号失败'})
        
        if(ress.affectedRows==1){
          res.send({
            code:200,
            message:'电源型号删除成功！'})
        }
  
        
    })
  
  })

// 批量删除处理器型号
router.post('/batchDeletePowerModel',(req,res)=>{
    console.log(req.body);
  
    const sqlurl="DELETE  FROM power_model where id=?"
  
    let nun=req.body.nunId
    let num=0
    nun.forEach((item)=>{
      db.query(sqlurl,item,(err,ress)=>{
     
        if(err) return res.send({
          code:404,
          message:'数据库批量删除处理器型号失败'})
          if(ress.affectedRows==1){
            num++
          
      if(num==nun.length){
  res.send({code:200,
    message:'批量删除成功!'})
      }
          }
      })
  
    })
  })















// 创建获取聊天对象的接口
module.exports = router;