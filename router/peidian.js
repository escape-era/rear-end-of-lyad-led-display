const express = require("express");
const router = express.Router();
const db = require("../mysql/index");

// 添加配电柜
router.post("/addpeidian", (req, res) => {

  
    // 插入用户语句
    //  插入前先判断是否已经有了相同的名称的
    const yong = "select model from peidian where model =? and openid = ?";
  
    db.query(yong, [req.body.name,req.user.id], (errs, rec) => {
      
      if (errs)
        return res.send({
          code: 404,
          message: "数据库配电柜插入请求失败",
        });
  
      // 已经存在品牌
      if (rec.length !== 0) {
        res.send({code: 201, message: "该配电柜线已经存在！"});
        // 不存在添加进去
      } else {
        const id = Math.floor(Math.random() * 1000000000) + 1;
        const charuUser =
          "insert into peidian (id,model,price,status,sort,openid) values (?,?,?,?,?,?)";

         
        db.query(
          charuUser,
          [id, req.body.model,req.body.price, req.body.status, req.body.sort,req.user.id],
          (err, resb) => {
            if (err)
              return res.send({
                code: 404,
                message: "数据库请求失败",
              });
  
            if (resb.affectedRows == 1) {
              res.send({code: 200, message: "配电柜添加成功！"});
            }
          }
        );
      }
    });
  });

  //   获取配电柜线

router.post("/obtaipeidian", (req, res) => {
    // 获取总行数
    let hang = 0;
    const zong = "SELECT COUNT(*) FROM peidian where openid = ?;";
    db.query(zong, req.user.id,(err, ress) => {
      hang = ress[0]["COUNT(*)"];
  
      // const sqlurl = "select * from control_card ORDER BY sort ASC";
      const sqlurlt =
        "select * from peidian where status like ? and model like ? and openid = ? ORDER BY sort ASC LIMIT ?,?";
      let nun = req.body;
  
      let cha = {};
  
      if (nun.input == "") {
        cha.input = "%";
      } else {
        cha.input = nun.input;
      }
      if (nun.status == 0) {
        cha.status = "%";
      } else {
        cha.status = nun.status;
      }
      // 从哪里开始查询
      cha.currentPage = nun.pageSize * (nun.currentPage - 1);
      db.query(
        sqlurlt,
        [cha.status, cha.input,req.user.id, cha.currentPage, nun.pageSize],
        (err, ress) => {
          if (err)
            return res.send({
              code: 404,
              message: "数据库请求失败",
            });
  
          if (ress.length !== 0) {
            res.send({
              code: 200,
              data: ress,
              message: "获取配电柜成功！",
              total: hang,
            });
          } else if (ress.length == 0) {
            res.send({
              code: 200,
              data: ress,
              total: hang,
              message: "获取配电柜成功！",
            });
          }
        }
      );
    });
  });


    // 删除配电柜
    router.post('/deletepeidian',(req,res)=>{


        const deletepin='delete from peidian where id=?'
        db.query(deletepin,req.body.id,(err,ress)=>{
    
          if(err) return res.send({
            code:404,
            message:'数据库删除配电柜线失败'})
            
            if(ress.affectedRows==1){
              res.send({
                code:200,
                message:'配电柜删除成功！'})
            }
    
            
        })
      })
// 编辑配电柜
router.post('/editpeidian',(req,res)=>{

    let nun=req.body
    const sqlurl = `update peidian set model=?,price=?,status=?,sort=? where id=?`;
    db.query(sqlurl,[nun.model,nun.price,nun.status,nun.sort,nun.id],(err,ress)=>{
    if(err) return res.send({
      code:404,
      message:'数据库编辑配电柜失败'})
    if(ress.affectedRows==1){
      res.send({
        code:200,
        message:'编辑成功!'
      })
    }
    })
    })
// 批量删除

router.post('/batchDeletepeidian',(req,res)=>{
    const sqlurl="DELETE  FROM peidian where id=?"
    
      let nun=req.body.nunId
      let num=0
      nun.forEach((item)=>{
        db.query(sqlurl,item,(err,ress)=>{
       
          if(err) return res.send({
            code:404,
            message:'数据库批量删除电源品牌失败'})
            if(ress.affectedRows==1){
              num++
            
        if(num==nun.length){
    res.send({code:200,
      message:'批量删除成功!'})
        }
            }
        })
    
      })
    })
// 创建获取聊天对象的接口
module.exports = router;