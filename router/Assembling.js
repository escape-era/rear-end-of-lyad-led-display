const express = require("express");
const router = express.Router();
const db = require("../mysql/index");


// 添加组装箱体
router.post('/addAssembling',(req,res)=>{
  
    // 插入用户语句
   //  插入前先判断是否已经有了相同的名称的
   const yong = "select model from ssembling_box where model =? and openid= ?";

   db.query(yong,[req.body.model,req.user.id],(errs,rec)=>{
   if(errs) return res.send({
     code:404,
     message:'数据库请求电源型号失败'})
   
   // 已经存在品牌
   if(rec.length!==0){
   res.send({code:201,
     message:'该型号已经存在！'})
   // 不存在添加进去
   }else{
  
     let nun=req.body
     console.log(nun);
    const id=Math.floor(Math.random()*1000000000)+1
    const charuUser = "insert into ssembling_box (id,model,price,brand_id,openid) values (?,?,?,?,?)";
     db.query(charuUser,[id,nun.model,nun.price,nun.brand_id,req.user.id],(err,resb)=>{
      if(err) return res.send({
       code:404,
       message:'数据库请求添加组装箱体型号失败'})
     if(resb.affectedRows==1){
       res.send({code:200,
         message:'组装箱体型号添加成功！'}) }})} }) })


// 获取组装箱体
router.post('/obtainAssembling',(req,res)=>{
    // 获取总行数
  let hang=0
  const zong='SELECT COUNT(*) FROM ssembling_box where openid = ? ;'
  db.query(zong,req.user.id,(err,ress)=>{
    hang=ress[0]['COUNT(*)']
  
      // const sqlurl = "select * from brand ORDER BY sort ASC";
      const sqlurlt = "select * from ssembling_box where brand_id like ? and model like ?  and openid = ?  LIMIT ?,?";
      let nun=req.body
    
      let cha={}
     
     if(nun.input==''){
     cha.input="%"
     
     }else{
       cha.input=nun.input
     }
     if(nun.status==0){
       cha.status="%"
     }else{
       cha.status=nun.status
     }
     
   
     // 从哪里开始查询
     cha.currentPage=nun.pageSize*(nun.currentPage-1)
     
       db.query(sqlurlt,[cha.status,cha.input,req.user.id,cha.currentPage,nun.pageSize],(err,ress)=>{
     
         if(err) return res.send({
           code:404,
           message:'数据库获取电源型号请求失败'})
     
           if(ress.length!==0){
     
             res.send({
               code:200,
               data:ress,
               message:'获取组装箱体成功！',
               total:hang
             })
           }else if(ress.length==0){
             res.send({
               code:200,
               data:ress,
               total:hang,
               message:'获取电源型号成功！'
             })
           }
       })
  })
  
  })


// 编辑组装箱体
router.post('/editAssembling',(req,res)=>{
    let nun=req.body
  const sqlurl = `update ssembling_box set model=?,price=?,brand_id=? where id=?`;
  db.query(sqlurl,[nun.model,nun.price,nun.brand_id,nun.id],(err,ress)=>{
    if(err) return res.send({
    code:404,
    message:'数据库编辑处理器型号失败'})
  if(ress.affectedRows==1){
    res.send({
      code:200,
      message:'编辑处理器型号成功!'
    })
  }
  })
  })

// 删除处理器型号
router.post('/deleteAssembling',(req,res)=>{

  
    const deletepin='delete from ssembling_box where id=?'
    db.query(deletepin,req.body.id,(err,ress)=>{
  
      if(err) return res.send({
        code:404,
        message:'数据库删除电源型号失败'})
        
        if(ress.affectedRows==1){
          res.send({
            code:200,
            message:'电源型号删除成功！'})
        }
  
        
    })
  
  })
// 批量删除处理器型号
router.post('/batchDeleteAssembling',(req,res)=>{
    console.log(req.body);
  
    const sqlurl="DELETE  FROM ssembling_box where id=?"
  
    let nun=req.body.nunId
    let num=0
    nun.forEach((item)=>{
      db.query(sqlurl,item,(err,ress)=>{
     
        if(err) return res.send({
          code:404,
          message:'数据库批量删除处理器型号失败'})
          if(ress.affectedRows==1){
            num++
          
      if(num==nun.length){
  res.send({code:200,
    message:'批量删除成功!'})
      }
          }
      })
  
    })
  })





// 创建获取聊天对象的接口
module.exports = router;