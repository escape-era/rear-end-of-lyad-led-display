const express = require("express");
const router = express.Router();
const db = require("../mysql/index");

// 添加电脑
router.post("/adddiannao", (req, res) => {

  
    // 插入用户语句
    //  插入前先判断是否已经有了相同的名称的
    const yong = "select model from diannao where model =? and openid = ?";
  
    db.query(yong, [req.body.name,req.user.id], (errs, rec) => {
      
      if (errs)
        return res.send({
          code: 404,
          message: "数据库电脑插入请求失败",
        });
  
      // 已经存在品牌
      if (rec.length !== 0) {
        res.send({code: 201, message: "该电脑线已经存在！"});
        // 不存在添加进去
      } else {
        const id = Math.floor(Math.random() * 1000000000) + 1;
        const charuUser =
          "insert into diannao (id,model,price,status,sort,openid) values (?,?,?,?,?,?)";

         
        db.query(
          charuUser,
          [id, req.body.model,req.body.price, req.body.status, req.body.sort,req.user.id],
          (err, resb) => {
            if (err)
              return res.send({
                code: 404,
                message: "数据库请求失败",
              });
  
            if (resb.affectedRows == 1) {
              res.send({code: 200, message: "电脑添加成功！"});
            }
          }
        );
      }
    });
  });

  //   获取电脑线

router.post("/obtaidiannao", (req, res) => {
    // 获取总行数
    let hang = 0;
    const zong = "SELECT COUNT(*) FROM diannao where openid = ?;";
    db.query(zong,req.user.id,(err, ress) => {
      hang = ress[0]["COUNT(*)"];
  
      // const sqlurl = "select * from control_card ORDER BY sort ASC";
      const sqlurlt =
        "select * from diannao where status like ? and model like ? and openid = ? ORDER BY sort ASC LIMIT ?,?";
      let nun = req.body;
  
      let cha = {};
  
      if (nun.input == "") {
        cha.input = "%";
      } else {
        cha.input = nun.input;
      }
      if (nun.status == 0) {
        cha.status = "%";
      } else {
        cha.status = nun.status;
      }
      // 从哪里开始查询
      cha.currentPage = nun.pageSize * (nun.currentPage - 1);
      db.query(
        sqlurlt,
        [cha.status, cha.input,req.user.id, cha.currentPage, nun.pageSize],
        (err, ress) => {
          if (err)
            return res.send({
              code: 404,
              message: "数据库请求失败",
            });
  
          if (ress.length !== 0) {
            res.send({
              code: 200,
              data: ress,
              message: "获取电脑成功！",
              total: hang,
            });
          } else if (ress.length == 0) {
            res.send({
              code: 200,
              data: ress,
              total: hang,
              message: "获取电脑成功！",
            });
          }
        }
      );
    });
  });


    // 删除电脑
    router.post('/deletediannao',(req,res)=>{


        const deletepin='delete from diannao where id=?'
        db.query(deletepin,req.body.id,(err,ress)=>{
    
          if(err) return res.send({
            code:404,
            message:'数据库删除电脑线失败'})
            
            if(ress.affectedRows==1){
              res.send({
                code:200,
                message:'电脑删除成功！'})
            }
    
            
        })
      })
// 编辑电脑
router.post('/editdiannao',(req,res)=>{

    let nun=req.body
    const sqlurl = `update diannao set model=?,price=?,status=?,sort=? where id=?`;
    db.query(sqlurl,[nun.model,nun.price,nun.status,nun.sort,nun.id],(err,ress)=>{
    if(err) return res.send({
      code:404,
      message:'数据库编辑电脑失败'})
    if(ress.affectedRows==1){
      res.send({
        code:200,
        message:'编辑成功!'
      })
    }
    })
    })
// 批量删除

router.post('/batchDeletediannao',(req,res)=>{
    const sqlurl="DELETE  FROM diannao where id=?"
    
      let nun=req.body.nunId
      let num=0
      nun.forEach((item)=>{
        db.query(sqlurl,item,(err,ress)=>{
       
          if(err) return res.send({
            code:404,
            message:'数据库批量删除电源品牌失败'})
            if(ress.affectedRows==1){
              num++
            
        if(num==nun.length){
    res.send({code:200,
      message:'批量删除成功!'})
        }
            }
        })
    
      })
    })
// 创建获取聊天对象的接口
module.exports = router;