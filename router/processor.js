const express = require("express");
const router = express.Router();
const db = require("../mysql/index");

// 获取处理器品牌

router.post("/obtainProcessor", (req, res) => {
  // 获取总行数
  let hang = 0;
  const zong = "SELECT COUNT(*) FROM processor where openid = ?;";
  db.query(zong,req.user.id, (err, ress) => {
    hang=ress?ress[0]['COUNT(*)']:0

    // const sqlurl = "select * from control_card ORDER BY sort ASC";
    const sqlurlt =
      "select * from processor where status like ? and name like ? and openid= ? ORDER BY sort ASC LIMIT ?,?";
    let nun = req.body;

    let cha = {};

    if (nun.input == "") {
      cha.input = "%";
    } else {
      cha.input = nun.input;
    }
    if (nun.status == 0) {
      cha.status = "%";
    } else {
      cha.status = nun.status;
    }
    // 从哪里开始查询
    cha.currentPage = nun.pageSize * (nun.currentPage - 1);
    db.query(
      sqlurlt,
      [cha.status, cha.input,req.user.id, cha.currentPage, nun.pageSize],
      (err, ress) => {
        if (err)
          return res.send({
            code: 404,
            message: "数据库获取处理器请求失败",
          });

        if (ress.length !== 0) {
          res.send({
            code: 200,
            data: ress,
            message: "获取处理器成功！",
            total: hang,
          });
        } else if (ress.length == 0) {
          res.send({
            code: 200,
            data: ress,
            total: hang,
            message: "获取处理器成功！",
          });
        }
      }
    );
  });
});

// 添加处理器品牌
router.post("/AddProcessor", (req, res) => {
  // 插入用户语句
  //  插入前先判断是否已经有了相同的名称的
  const yong = "select name from processor where name =? and openid= ?";

  db.query(yong,[ req.body.name,req.user.id], (errs, rec) => {
    if (errs)
      return res.send({
        code: 404,
        message: "数据库添加处理器失败",
      });

    // 已经存在品牌
    if (rec.length !== 0) {
      res.send({code: 201, message: "该品牌已经存在！"});
      // 不存在添加进去
    } else {
      const id = Math.floor(Math.random() * 1000000000) + 1;
      const charuUser =
        "insert into processor (id,name,status,sort,openid) values (?,?,?,?,?)";
      db.query(
        charuUser,
        [id, req.body.name, req.body.status, req.body.sort,req.user.id],
        (err, resb) => {
          if (err)
            return res.send({
              code: 404,
              message: "数据库请求失败",
            });

          if (resb.affectedRows == 1) {
            res.send({code: 200, message: "处理器品牌添加成功！"});
          }
        }
      );
    }
  });
});

     // 删除处理器品牌
     router.post('/deleteProcessor',(req,res)=>{


        const deletepin='delete from processor where id=?'
        db.query(deletepin,req.body.id,(err,ress)=>{
    
          if(err) return res.send({
            code:404,
            message:'数据库删除品牌失败'})
            
            if(ress.affectedRows==1){
              res.send({
                code:200,
                message:'处理器品牌删除成功！'})
            }
    
            
        })
      })

// 编辑控制卡品牌
router.post('/editProcessor',(req,res)=>{

    let nun=req.body
    const sqlurl = `update processor set name=?,status=?,sort=? where id=?`;
    db.query(sqlurl,[nun.name,nun.status,nun.sort,nun.id],(err,ress)=>{
    if(err) return res.send({
      code:404,
      message:'数据库编辑处理器品牌失败'})
    if(ress.affectedRows==1){
      res.send({
        code:200,
        message:'编辑成功!'
      })
    }
    })
    })

// 批量删除处理器品牌
router.post('/batchDeletetProcessor',(req,res)=>{
    const sqlurl="DELETE  FROM processor where id=?"
    
      let nun=req.body.nunId
      let num=0
      nun.forEach((item)=>{
        db.query(sqlurl,item,(err,ress)=>{
       
          if(err) return res.send({
            code:404,
            message:'数据库编辑处理器品牌失败'})
            if(ress.affectedRows==1){
              num++
            
        if(num==nun.length){
    res.send({code:200,
      message:'批量删除成功!'})
        }
            }
        })
    
      })
    })


// 添加处理器型号
router.post('/addProcessorModel',(req,res)=>{
  
    // 插入用户语句
   //  插入前先判断是否已经有了相同的名称的
   const yong = "select model from processor_model where model =? and openid = ?";
   
   console.log(req.body);
   db.query(yong,[req.body.model,req.user.id],(errs,rec)=>{
   if(errs) return res.send({
     code:404,
     message:'数据库请求处理器型号失败'})
   
   // 已经存在品牌
   if(rec.length!==0){
   res.send({code:201,
     message:'该型号已经存在！'})
   // 不存在添加进去
   }else{
   
     let nun=req.body
    const id=Math.floor(Math.random()*1000000000)+1
    const charuUser = "insert into processor_model (id,model,price,brand_id,openid) values (?,?,?,?,?)";
     db.query(charuUser,[id,nun.model,nun.price,nun.brand_id,req.user.id],(err,resb)=>{
      if(err) return res.send({
       code:404,
       message:'数据库请求添加处理器型号失败'})
     if(resb.affectedRows==1){
       res.send({code:200,
         message:'处理器型号添加成功！'}) }})} }) })


// 获取处理器型号
router.post('/obtainProcessorModel',(req,res)=>{
    // 获取总行数
  let hang=0
  const zong='SELECT COUNT(*) FROM processor_model where openid = ?;'
  db.query(zong,req.user.id,(err,ress)=>{
    hang=ress?ress[0]['COUNT(*)']:0
  
      // const sqlurl = "select * from brand ORDER BY sort ASC";
      const sqlurlt = "select * from processor_model where brand_id like ? and model like ? and openid = ?  LIMIT ?,?";
      let nun=req.body
    
      let cha={}
     
     if(nun.input==''){
     cha.input="%"
     
     }else{
       cha.input=nun.input
     }
     if(nun.status==0){
       cha.status="%"
     }else{
       cha.status=nun.status
     }
    
   
     // 从哪里开始查询
     cha.currentPage=nun.pageSize*(nun.currentPage-1)
     
       db.query(sqlurlt,[cha.status,cha.input,req.user.id,cha.currentPage,nun.pageSize],(err,ress)=>{
     
         if(err) return res.send({
           code:404,
           message:'数据库获取控制卡型号请求失败'})
     
           if(ress.length!==0){
     
             res.send({
               code:200,
               data:ress,
               message:'获取处理器型号成功！',
               total:hang
             })
           }else if(ress.length==0){
             res.send({
               code:200,
               data:ress,
               total:hang,
               message:'获取处理器型号成功！'
             })
           }
       })
  })
  
  })
// 编辑处理器型号
router.post('/editProcessorModel',(req,res)=>{


    let nun=req.body
  const sqlurl = `update processor_model set model=?,price=?,brand_id=? where id=?`;
  db.query(sqlurl,[nun.model,nun.price,nun.brand_id,nun.id],(err,ress)=>{
  
  
    if(err) return res.send({
    code:404,
    message:'数据库编辑处理器型号失败'})
  if(ress.affectedRows==1){
    res.send({
      code:200,
      message:'编辑处理器型号成功!'
    })
  }
  })
  })

// 删除处理器型号
router.post('/deleteProcessorModel',(req,res)=>{

  
    const deletepin='delete from processor_model where id=?'
    db.query(deletepin,req.body.id,(err,ress)=>{
  
      if(err) return res.send({
        code:404,
        message:'数据库删除处理器型号失败'})
        
        if(ress.affectedRows==1){
          res.send({
            code:200,
            message:'处理器型号删除成功！'})
        }
  
        
    })
  
  })


// 批量删除处理器型号
router.post('/batchDeleteProcessorModel',(req,res)=>{
    console.log(req.body);
  
    const sqlurl="DELETE  FROM processor_model where id=?"
  
    let nun=req.body.nunId
    let num=0
    nun.forEach((item)=>{
      db.query(sqlurl,item,(err,ress)=>{
     
        if(err) return res.send({
          code:404,
          message:'数据库批量删除处理器型号失败'})
          if(ress.affectedRows==1){
            num++
          
      if(num==nun.length){
  res.send({code:200,
    message:'批量删除成功!'})
      }
          }
      })
  
    })
  })
  

















// 创建获取聊天对象的接口
module.exports = router;
