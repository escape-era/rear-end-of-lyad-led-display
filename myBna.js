const express = require("express");
const app = express();
const userRouter = require("./router/user");
const path = require("path");
const xlsx = require('xlsx');
let cors = require("cors");
const myUser = require("./router/myUser");
const processor=require('./router/processor')
const Controlcard=require('./router/Controlcard')
const box=require('./router/box')
const secretKey = require("./router/confing");
const secretKeyhou = require("./router/confing");
const power=require('./router/power')
const taxRate=require('./router/taxRate')
const Network=require('./router/Network')
const paixian =require('./router/paixian')
const Assembling=require('./router/Assembling')
const framework=require('./router/framework')
const dorsal=require('./router/dorsal')
const xiaochengx=require('./router/xiaochengx')
const heihong=require('./router/heihong')
const peidian= require('./router/peidian')
const diannao=require("./router/diannao")
const anzhaung =require('./router/anzhuang')
const kongtiao =require("./router/kongtiao")
const fs = require("fs");
const ExcelJS = require('exceljs');
const chuang=require('./router/unils/exlx')
var bodyParser = require("body-parser");
// const jwt =require('jsonwebtoken')
const expressTwt = require("express-jwt");





// app.get('/excel', (req, res) => {
//   // 生成 Excel 文件
//   const wb = xlsx.utils.book_new();
//   const ws = xlsx.utils.json_to_sheet([{ name: 'John Doe', age: 30 }]);
//   xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
//   const data = xlsx.write(wb, { bookType: 'xlsx', type: 'buffer' });

//   // 返回 Excel 文件
//   res.set({
//     'Content-Type': 'application/octet-stream',
//     'Content-Disposition': 'attachment; filename=test.xlsx',
//   });
//   res.send(data);
// });



app.use(express.static('public'));

// app.post('/meng/gerisd',chuang)


// 解析



// 解析 application/json
// 配置解析数据
app.use(bodyParser.json({ limit: "5500000000kb" }));
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(bodyParser.urlencoded({ extended: false }));//解析post请求数据
// 配置跨域
app.use(cors())
// 配置解析token
// app.use(expressTwt({ secret: secretKeyhou.secretKeyhou }).unless({ path: [/^\/lan\//] }));

// /^\/meng\//
app.use(expressTwt({ secret: secretKey.secretKey }).unless({ path: [/^\/lan\//] }));

// 注册路由
app.use("/lan", userRouter);
app.use("/meng",xiaochengx)
// 注册需要token的路由
app.use("/meng", myUser);
app.use("/meng",Controlcard)
app.use("/meng",processor)
app.use("/meng",power)
app.use("/meng",box)
app.use("/meng",Assembling)
app.use("/meng",framework)
app.use("/meng",Network)
app.use("/meng",taxRate)
app.use("/meng",heihong)
app.use("/meng",paixian)
app.use("/meng",peidian)
app.use("/meng",dorsal)
app.use("/meng",diannao)
app.use("/meng",kongtiao)
app.use("/meng",anzhaung)
app.use((err, req, res, next) => {
  if (err.name === "UnauthorizedError") {
    return res.send({
      status: 401,
      message: "无效的token",
    });
  }
  res.send({
    status: 500,
    message: "未知的错误",
  });

  // 打印错误日志
  const err_log = `
        +---------------+-------------------------+
        错误名称：${err.name},\n
        错误信息：${err.message},\n
        错误时间：${new Date()},\n
        错误堆栈：${err.stack},\n
        +---------------+-------------------------+
    `;
  // fs.appendFile(path.join(__dirname, "error.log"), err_log, () => {
  //   res.writeHead(500, { "Content-Type": "text/html;charset=utf-8" });
  //   res.end(`500 服务器内部错误`);
  // });

  next();
});

app.listen(3005, (res) => {
  console.log("服务启动成功");
});


